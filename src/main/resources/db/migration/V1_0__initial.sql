CREATE TABLE accounts (
  id         INT AUTO_INCREMENT PRIMARY KEY,
  account_id VARCHAR(32) UNIQUE,
  password   VARCHAR(128)
);

CREATE SEQUENCE short_code_seq START WITH 100000;

CREATE TABLE short_urls (
  short_code    VARCHAR(16) PRIMARY KEY,
  owner         INT REFERENCES accounts (id) ON DELETE CASCADE,
  url           VARCHAR,
  redirect_type INT CHECK (redirect_type IN (301, 302)),
  statistic     BIGINT DEFAULT 0
);