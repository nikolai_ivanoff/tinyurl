package ru.ind.infobip;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TinyurlApplication {

    public static void main(final String[] args) {
        SpringApplication.run(TinyurlApplication.class, args);
    }
}
