package ru.ind.infobip.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.ind.infobip.services.shortcodegen.ShortCodeGenerator;
import ru.ind.infobip.services.shortcodegen.SimpleShortCodeGenerator;

@Configuration
public class Config {
    @Bean
    public ShortCodeGenerator shortCodeGenerator() {
        return new SimpleShortCodeGenerator();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}