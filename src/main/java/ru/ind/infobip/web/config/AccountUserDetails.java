package ru.ind.infobip.web.config;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.ind.infobip.services.accountregistration.model.Account;

import java.util.Collection;
import java.util.Collections;

public class AccountUserDetails implements UserDetails {
    private static final long serialVersionUID = -5142389860865844899L;
    private static final Collection<? extends GrantedAuthority> AUTHORITIES = Collections.singletonList(new SimpleGrantedAuthority("ACCOUNT"));

    private final Account account;

    public AccountUserDetails(final Account account) {
        this.account = account;
    }

    public Account getAccount() {
        return account;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AUTHORITIES;
    }

    @Override
    public String getPassword() {
        return account.getPassword();
    }

    @Override
    public String getUsername() {
        return account.getAccountId();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String toString() {
        return String.format("AccountUserDetails{%s}", account);
    }
}