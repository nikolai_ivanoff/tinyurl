package ru.ind.infobip.web.controllers.shorturl.dto;

import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.URL;

public class ShortUrlRegisterRequest {
    private String url;

    @Range(min = 301, max = 302)
    private int redirectType = 302;

    @URL
    public String getUrl() {
        if (url != null) {
            final String tmp = url.toLowerCase();
            if (tmp.startsWith("http://") || tmp.startsWith("https://")) {
                return url;
            } else {
                return "http://" + url;
            }
        } else {
            return null;
        }
    }

    public int getRedirectType() {
        return redirectType;
    }
}