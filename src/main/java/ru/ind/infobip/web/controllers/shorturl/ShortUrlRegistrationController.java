package ru.ind.infobip.web.controllers.shorturl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.ind.infobip.services.shorturl.ShortUrlService;
import ru.ind.infobip.services.shorturl.model.ShortUrl;
import ru.ind.infobip.web.config.AccountUserDetails;
import ru.ind.infobip.web.controllers.shorturl.dto.ShortUrlRegisterRequest;
import ru.ind.infobip.web.controllers.shorturl.dto.ShortUrlRegisterResponse;

import javax.inject.Inject;
import javax.validation.Valid;

@RestController
public class ShortUrlRegistrationController {
    private final ShortUrlService shortUrlService;

    /**
     * Base URL part appended by short code
     */
    @Value("${short-url-prefix}")
    private String shortUrlPrefix;

    @Inject
    public ShortUrlRegistrationController(final ShortUrlService shortUrlService) {
        this.shortUrlService = shortUrlService;
    }

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public ResponseEntity<ShortUrlRegisterResponse> registerUrl(@Valid @RequestBody final ShortUrlRegisterRequest request,
                                                                @AuthenticationPrincipal final AccountUserDetails userDetails) {

        final ShortUrl shortUrl = shortUrlService.register(userDetails.getAccount(), request.getUrl(), request.getRedirectType());
        final String url = shortUrlPrefix + shortUrl.getShortCode();
        return new ResponseEntity<>(new ShortUrlRegisterResponse(url), HttpStatus.CREATED);
    }
}