package ru.ind.infobip.web.controllers.account.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

public class AccountRequest {
    @NotEmpty
    @JsonProperty("AccountId")
    private String accountId;

    public AccountRequest() {
    }

    public AccountRequest(final String accountId) {
        this.accountId = accountId;
    }

    public String getAccountId() {
        return accountId;
    }
}