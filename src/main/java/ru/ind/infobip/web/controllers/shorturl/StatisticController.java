package ru.ind.infobip.web.controllers.shorturl;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.ind.infobip.services.accountregistration.AccountService;
import ru.ind.infobip.services.accountregistration.model.Account;
import ru.ind.infobip.services.shorturl.ShortUrlService;
import ru.ind.infobip.web.controllers.ResourceNotFoundException;

import javax.inject.Inject;
import java.util.Map;

@RestController
public class StatisticController {
    private final AccountService accountService;
    private final ShortUrlService shortUrlService;

    @Inject
    public StatisticController(final AccountService accountService, final ShortUrlService shortUrlService) {
        this.accountService = accountService;
        this.shortUrlService = shortUrlService;
    }

    @RequestMapping(path = "/statistic/{accountId}", method = RequestMethod.GET)
    public Map<String, Long> statistic(@PathVariable("accountId") final String accountId) {
        final Account account = accountService.findAccount(accountId)
                .orElseThrow(() -> new ResourceNotFoundException("Account " + accountId + " not found"));

        return shortUrlService.getStatisticByOwner(account);
    }
}