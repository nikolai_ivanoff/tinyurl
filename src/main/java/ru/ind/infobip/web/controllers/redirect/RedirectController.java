package ru.ind.infobip.web.controllers.redirect;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.view.RedirectView;
import ru.ind.infobip.services.shorturl.ShortUrlService;
import ru.ind.infobip.services.shorturl.model.ShortUrl;
import ru.ind.infobip.web.controllers.ResourceNotFoundException;
import springfox.documentation.annotations.ApiIgnore;

import javax.inject.Inject;

@Controller
@ApiIgnore
public class RedirectController {
    private final ShortUrlService shortUrlService;

    @Inject
    public RedirectController(final ShortUrlService shortUrlService) {
        this.shortUrlService = shortUrlService;
    }

    @RequestMapping(path = "/{shortUrlCode:^(?!(?:help|register|statistic|account|swagger-ui.html|v2)).*}", method = RequestMethod.GET)
    public RedirectView redirect(@PathVariable("shortUrlCode") final String shortUrlCode) {
        return shortUrlService.findByShortUrlCode(shortUrlCode)
                .map(shortUrl -> {
                    shortUrlService.registerRedirect(shortUrl.getShortCode());
                    return createRedirect(shortUrl);
                })
                .orElseThrow(ResourceNotFoundException::new);
    }

    private static RedirectView createRedirect(final ShortUrl shortUrl) {
        final RedirectView rv = new RedirectView(shortUrl.getUrl());
        rv.setStatusCode(HttpStatus.valueOf(shortUrl.getRedirectType()));
        return rv;
    }
}