package ru.ind.infobip.web.controllers.shorturl.dto;

public class ShortUrlRegisterResponse {
    private String shortUrl;

    public ShortUrlRegisterResponse(final String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public String getShortUrl() {
        return shortUrl;
    }
}