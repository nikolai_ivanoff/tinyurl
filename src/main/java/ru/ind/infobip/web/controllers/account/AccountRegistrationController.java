package ru.ind.infobip.web.controllers.account;

import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.ind.infobip.services.accountregistration.AccountAlreadyExistsException;
import ru.ind.infobip.services.accountregistration.AccountService;
import ru.ind.infobip.services.accountregistration.model.Account;
import ru.ind.infobip.web.controllers.account.dto.AccountRequest;
import ru.ind.infobip.web.controllers.account.dto.AccountResponse;

import javax.inject.Inject;
import javax.validation.Valid;

@RestController
public class AccountRegistrationController {
    private final AccountService accountService;

    @Inject
    public AccountRegistrationController(final AccountService accountService) {
        this.accountService = accountService;
    }

    @ApiOperation(value = "Opening of accounts")
    @RequestMapping(path = "/account", method = RequestMethod.POST)
    public ResponseEntity<AccountResponse> registerAccount(@Valid @RequestBody final AccountRequest accountRequest) {
        final Account account = accountService.registerAccount(accountRequest.getAccountId());
        final AccountResponse success = AccountResponse.success(account.getRawPassword());
        return new ResponseEntity<>(success, HttpStatus.CREATED);
    }

    @ExceptionHandler(value = AccountAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public AccountResponse handleException(final AccountAlreadyExistsException exception) {
        return AccountResponse.fail(exception.getMessage());
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public AccountResponse handleException(final Exception exception) {
        return AccountResponse.fail(exception);
    }
}