package ru.ind.infobip.web.controllers.account.dto;

public class AccountResponse {
    private final boolean success;
    private final String description;
    private final String password;

    private AccountResponse(final boolean success, final String description, final String password) {
        this.success = success;
        this.description = description;
        this.password = password;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getDescription() {
        return description;
    }

    public String getPassword() {
        return password;
    }

    public static AccountResponse success(final String password) {
        return new AccountResponse(true, "Your account is opened", password);
    }

    public static AccountResponse fail(final Exception exception) {
        return new AccountResponse(false, exception.toString(), null);
    }

    public static AccountResponse fail(final String description) {
        return new AccountResponse(false, description, null);
    }
}