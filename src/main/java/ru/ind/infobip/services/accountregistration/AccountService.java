package ru.ind.infobip.services.accountregistration;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ind.infobip.services.accountregistration.dao.AccountDao;
import ru.ind.infobip.services.accountregistration.model.Account;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.util.Objects;
import java.util.Optional;

@Service
public class AccountService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountService.class);

    private final AccountDao accountRegistrationDao;

    private final PasswordEncoder passwordEncoder;

    /**
     * Account generated password length
     */
    @Value("${account.password-length}")
    private int passwordLength;

    @Inject
    public AccountService(final AccountDao accountRegistrationDao, final PasswordEncoder passwordEncoder) {
        this.accountRegistrationDao = accountRegistrationDao;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * Generate password and register new account
     *
     * @return registered account with generated password
     */
    @Transactional
    @Nonnull
    public Account registerAccount(@Nonnull final String accountId) {
        Objects.requireNonNull(accountId, "Account id should be not null");

        if (accountRegistrationDao.findAccount(accountId).isPresent()) {
            throw new AccountAlreadyExistsException("Account " + accountId + " already exists");
        }

        final String rawPasswd = RandomStringUtils.randomAlphabetic(passwordLength);
        final String encodedPasswd = passwordEncoder.encode(rawPasswd);

        final Account account = accountRegistrationDao.insert(accountId, encodedPasswd).setRawPassword(rawPasswd);

        LOGGER.info("Register account {}", account);

        return account;
    }

    /**
     * Search account by accountId in DB
     */
    @Transactional
    @Nonnull
    public Optional<Account> findAccount(final String accountId) {
        final Optional<Account> account = accountRegistrationDao.findAccount(accountId);

        if (account.isPresent()) {
            LOGGER.trace("Account {} found: {}", accountId, account.get());
        } else {
            LOGGER.trace("Account {} not found", accountId);
        }

        return account;
    }
}