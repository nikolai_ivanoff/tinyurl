package ru.ind.infobip.services.accountregistration.dao;

import org.jooq.impl.DefaultDSLContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.ind.infobip.jooq.tables.records.AccountsRecord;
import ru.ind.infobip.services.accountregistration.model.Account;

import javax.inject.Inject;
import java.util.Optional;

import static ru.ind.infobip.jooq.Tables.ACCOUNTS;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class AccountDao {
    private final DefaultDSLContext jooq;

    @Inject
    public AccountDao(final DefaultDSLContext jooq) {
        this.jooq = jooq;
    }

    public Account insert(final String accountId, final String password) {
        final AccountsRecord record = jooq.insertInto(ACCOUNTS)
                .set(ACCOUNTS.ACCOUNT_ID, accountId)
                .set(ACCOUNTS.PASSWORD, password)
                .returning(ACCOUNTS.ID)
                .fetchOne();

        return new Account(record.getValue(ACCOUNTS.ID), accountId, password);
    }

    public Optional<Account> findAccount(final String accountId) {
        return jooq.selectFrom(ACCOUNTS)
                .where(ACCOUNTS.ACCOUNT_ID.eq(accountId))
                .fetchOptional(r -> new Account(r.get(ACCOUNTS.ID), r.get(ACCOUNTS.ACCOUNT_ID), r.get(ACCOUNTS.PASSWORD)));
    }
}