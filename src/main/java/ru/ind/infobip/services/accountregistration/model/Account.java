package ru.ind.infobip.services.accountregistration.model;

import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.Objects;

public class Account implements Serializable {
    private static final long serialVersionUID = -329173327973133415L;

    private final int id;

    private final String accountId;

    /**
     * encrypted password (hash)
     */
    private final String password;

    /**
     * raw password
     */
    @Nullable
    private String rawPassword;

    public Account(final int id, final String accountId, final String password) {
        this.id = id;
        this.accountId = accountId;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public String getAccountId() {
        return accountId;
    }

    public String getPassword() {
        return password;
    }

    @Nullable
    public String getRawPassword() {
        return rawPassword;
    }

    public Account setRawPassword(@Nullable final String rawPassword) {
        this.rawPassword = rawPassword;
        return this;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        } else if (o == null || getClass() != o.getClass()) {
            return false;
        } else {
            final Account account = (Account) o;
            return Objects.equals(accountId, account.accountId);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId);
    }

    @Override
    public String toString() {
        return String.format("{accountId='%s'}", accountId);
    }
}