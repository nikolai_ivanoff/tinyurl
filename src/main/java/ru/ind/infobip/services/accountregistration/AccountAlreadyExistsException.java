package ru.ind.infobip.services.accountregistration;

public class AccountAlreadyExistsException extends RuntimeException {
    private static final long serialVersionUID = 1466173860346733178L;

    public AccountAlreadyExistsException(final String message) {
        super(message);
    }
}
