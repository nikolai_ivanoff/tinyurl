package ru.ind.infobip.services.shortcodegen;

import javax.annotation.Nonnull;

public class SimpleShortCodeGenerator implements ShortCodeGenerator {
    @Nonnull
    @Override
    public String generate(final long value) {
        return Long.toString(value, Character.MAX_RADIX);
    }
}