package ru.ind.infobip.services.shortcodegen;

import javax.annotation.Nonnull;

/**
 * Generate short code from long value
 */
public interface ShortCodeGenerator {
    @Nonnull
    String generate(long value);
}
