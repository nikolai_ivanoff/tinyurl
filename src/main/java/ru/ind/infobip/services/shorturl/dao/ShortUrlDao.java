package ru.ind.infobip.services.shorturl.dao;

import org.jooq.impl.DefaultDSLContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.ind.infobip.services.accountregistration.model.Account;
import ru.ind.infobip.services.shorturl.model.ShortUrl;

import javax.inject.Inject;
import java.util.Map;
import java.util.Optional;

import static org.jooq.impl.DSL.sum;
import static ru.ind.infobip.jooq.Sequences.SHORT_CODE_SEQ;
import static ru.ind.infobip.jooq.Tables.SHORT_URLS;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class ShortUrlDao {
    private final DefaultDSLContext jooq;

    @Inject
    public ShortUrlDao(final DefaultDSLContext jooq) {
        this.jooq = jooq;
    }

    public long generateShortCodeId() {
        return jooq.select(SHORT_CODE_SEQ.nextval()).fetchOne().value1();
    }

    public ShortUrl register(final Account account, final String url, final String shortCode, final int redirectType) {
        jooq.insertInto(SHORT_URLS)
                .set(SHORT_URLS.SHORT_CODE, shortCode)
                .set(SHORT_URLS.OWNER, account.getId())
                .set(SHORT_URLS.URL, url)
                .set(SHORT_URLS.REDIRECT_TYPE, redirectType)
                .execute();

        return new ShortUrl(shortCode, account.getId(), url, redirectType, 0);
    }

    public Optional<ShortUrl> findByShortCode(final String shortCode) {
        return jooq.selectFrom(SHORT_URLS)
                .where(SHORT_URLS.SHORT_CODE.eq(shortCode))
                .fetchOptional(r -> new ShortUrl(
                        r.get(SHORT_URLS.SHORT_CODE),
                        r.get(SHORT_URLS.OWNER),
                        r.get(SHORT_URLS.URL),
                        r.get(SHORT_URLS.REDIRECT_TYPE),
                        r.get(SHORT_URLS.STATISTIC)
                ));
    }

    public void registerRedirect(final String shortCode) {
        jooq.update(SHORT_URLS)
                .set(SHORT_URLS.STATISTIC, SHORT_URLS.STATISTIC.add(1))
                .where(SHORT_URLS.SHORT_CODE.eq(shortCode))
                .execute();
    }

    public Map<String, Long> getStatisticByOwner(final Account account) {
        return jooq.select(SHORT_URLS.URL, sum(SHORT_URLS.STATISTIC).cast(Long.class))
                .from(SHORT_URLS)
                .where(SHORT_URLS.OWNER.eq(account.getId()))
                .groupBy(SHORT_URLS.URL)
                .fetchMap(SHORT_URLS.URL, sum(SHORT_URLS.STATISTIC).cast(Long.class));
    }
}