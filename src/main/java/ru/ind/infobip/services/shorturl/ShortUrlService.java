package ru.ind.infobip.services.shorturl;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ind.infobip.services.accountregistration.model.Account;
import ru.ind.infobip.services.shortcodegen.ShortCodeGenerator;
import ru.ind.infobip.services.shorturl.dao.ShortUrlDao;
import ru.ind.infobip.services.shorturl.model.ShortUrl;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@Service
public class ShortUrlService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ShortUrlService.class);

    private final ShortUrlDao shortUrlDao;

    private final ShortCodeGenerator shortCodeGenerator;

    private final Set<String> bannedCodes = ImmutableSet.of("register", "help", "account", "statistic", "swagger-ui.html", "v2");

    @Inject
    public ShortUrlService(final ShortUrlDao shortUrlDao, final ShortCodeGenerator shortCodeGenerator) {
        this.shortUrlDao = shortUrlDao;
        this.shortCodeGenerator = shortCodeGenerator;
    }

    /**
     * Register new short url
     *
     * @param account      account owner
     * @param url          long-url
     * @param redirectType redirect type (301 or 302)
     * @return
     */
    @Transactional
    @Nonnull
    public ShortUrl register(@Nonnull final Account account, @Nonnull final String url, final int redirectType) {
        Objects.requireNonNull(account, "Account should be not null");
        Objects.requireNonNull(url, "Url should be not null");
        Preconditions.checkArgument(redirectType == 301 || redirectType == 302, "Redirect type should be 301 or 302");

        final ShortUrl shortUrl = shortUrlDao.register(account, url, generateShortCode(), redirectType);
        LOGGER.info("Registered short url: {}", shortUrl);
        return shortUrl;
    }

    @Transactional
    @Nonnull
    public Optional<ShortUrl> findByShortUrlCode(@Nonnull final String shortUrlCode) {
        final Optional<ShortUrl> shortUrl = shortUrlDao.findByShortCode(shortUrlCode);

        if (shortUrl.isPresent()) {
            LOGGER.trace("Found short url: {}", shortUrlCode);
        } else {
            LOGGER.trace("Short url {} not found", shortUrlCode);
        }

        return shortUrl;
    }

    @Transactional
    public void registerRedirect(@Nonnull final String shortCode) {
        Objects.requireNonNull(shortCode, "Short code should be not null");

        shortUrlDao.registerRedirect(shortCode);
        LOGGER.debug("Register redirect for {}", shortCode);
    }

    @Transactional
    public Map<String, Long> getStatisticByOwner(final Account account) {
        LOGGER.trace("Get statistic for {}", account);
        return shortUrlDao.getStatisticByOwner(account);
    }

    @Transactional
    private String generateShortCode() {
        while (true) {
            final long id = shortUrlDao.generateShortCodeId();
            final String shortCode = shortCodeGenerator.generate(id);

            if (!bannedCodes.contains(shortCode)) {
                return shortCode;
            }
        }
    }
}
