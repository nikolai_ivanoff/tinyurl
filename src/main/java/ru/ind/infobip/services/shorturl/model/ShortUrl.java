package ru.ind.infobip.services.shorturl.model;

public class ShortUrl {
    private final String shortCode;
    private final int ownerId;
    private final String url;
    private final int redirectType;
    private final long statistic;

    public ShortUrl(final String shortCode, final int ownerId, final String url, final int redirectType, final long statistic) {
        this.shortCode = shortCode;
        this.ownerId = ownerId;
        this.url = url;
        this.redirectType = redirectType;
        this.statistic = statistic;
    }

    public String getShortCode() {
        return shortCode;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public String getUrl() {
        return url;
    }

    public int getRedirectType() {
        return redirectType;
    }

    public long getStatistic() {
        return statistic;
    }

    @Override
    public String toString() {
        return String.format("{'%s' => '%s'}", shortCode, url);
    }
}
