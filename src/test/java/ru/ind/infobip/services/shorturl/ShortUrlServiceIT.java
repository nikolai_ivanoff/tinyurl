package ru.ind.infobip.services.shorturl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.ind.infobip.services.accountregistration.AccountService;
import ru.ind.infobip.services.accountregistration.model.Account;
import ru.ind.infobip.services.shorturl.dao.ShortUrlDao;
import ru.ind.infobip.services.shorturl.model.ShortUrl;

import javax.inject.Inject;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "account.password-length=16")
public class ShortUrlServiceIT {
    @Inject
    private ShortUrlService shortUrlService;

    @Inject
    private AccountService accountService;

    @SpyBean
    private ShortUrlDao shortUrlDao;

    private Account account;

    @Before
    public void setup() {
        if (account == null) {
            account = accountService.registerAccount("test");
        }
    }

    @Test
    @Transactional
    public void register() throws Exception {
        final String url = "http://google.com";
        final int redirectType = 301;
        final ShortUrl shortUrl = shortUrlService.register(account, url, redirectType);

        assertThat(shortUrl.getShortCode())
                .isNotBlank();
        assertThat(shortUrl.getRedirectType())
                .isEqualTo(redirectType);
        assertThat(shortUrl.getOwnerId())
                .isEqualTo(account.getId());
        assertThat(shortUrl.getStatistic())
                .isEqualTo(0);

        verify(shortUrlDao)
                .register(eq(account), eq(url), any(String.class), eq(redirectType));
    }

    @Test
    @Transactional
    public void findByShortUrlCode() throws Exception {
        final ShortUrl shortUrl = shortUrlService.register(account, "http://google.com", 302);

        assertThat(shortUrlService.findByShortUrlCode(shortUrl.getShortCode()))
                .isPresent()
                .hasValueSatisfying(s -> {
                    assertThat(s).isEqualToComparingFieldByFieldRecursively(shortUrl);
                });
    }

    @Test
    @Transactional
    public void registerRedirect() throws Exception {
        final ShortUrl shortUrl = shortUrlService.register(account, "http://google.com", 302);

        shortUrlService.registerRedirect(shortUrl.getShortCode());
        shortUrlService.registerRedirect(shortUrl.getShortCode());
        shortUrlService.registerRedirect(shortUrl.getShortCode());

        assertThat(shortUrlService.findByShortUrlCode(shortUrl.getShortCode()))
                .isPresent()
                .hasValueSatisfying(s -> {
                    assertThat(s.getStatistic()).isEqualTo(3);
                });
    }
}