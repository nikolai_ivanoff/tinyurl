package ru.ind.infobip.services.shorturl;

import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.db.type.Request;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.ind.infobip.services.accountregistration.dao.AccountDao;
import ru.ind.infobip.services.accountregistration.model.Account;
import ru.ind.infobip.services.shorturl.dao.ShortUrlDao;
import ru.ind.infobip.services.shorturl.model.ShortUrl;

import javax.inject.Inject;
import javax.sql.DataSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;
import static org.assertj.db.api.Assertions.assertThat;
import static ru.ind.infobip.TestUtil.currentTransactionDataSource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ShortUrlDaoIT {
    @Inject
    private ShortUrlDao shortUrlDao;

    @Inject
    private AccountDao accountRegistrationDao;

    @Inject
    private DataSource dataSource;

    private Account account;

    @Before
    public void setup() {
        if (account == null) {
            account = accountRegistrationDao.insert("test", "passwd");
        }
    }

    @Test
    @Transactional
    public void register() {
        final String url = RandomStringUtils.randomAlphanumeric(20);
        final String shortCode = "xyswle";
        final int redirectType = 301;

        ShortUrl shortUrl = shortUrlDao.register(account, url, shortCode, redirectType);

        assertThat(shortUrl)
                .isNotNull()
                .satisfies(u -> {
                    assertThat(u.getShortCode())
                            .isEqualTo(shortCode);
                    assertThat(u.getUrl())
                            .isEqualTo(url);
                    assertThat(u.getOwnerId())
                            .isEqualTo(account.getId());
                    assertThat(u.getStatistic())
                            .isZero();
                });

        assertThat(new Request(currentTransactionDataSource(dataSource), "SELECT * FROM short_urls WHERE short_code=?", shortUrl.getShortCode()))
                .hasNumberOfRows(1)
                .column("owner").value().isEqualTo(account.getId())
                .column("url").value().isEqualTo(url)
                .column("redirect_type").value().isEqualTo(redirectType)
                .column("statistic").value().isEqualTo(0);
    }

    @Test
    @Transactional
    public void findById() {
        final String url = RandomStringUtils.randomAlphanumeric(20);
        ShortUrl shortUrl = shortUrlDao.register(account, url, "buGei0id", 301);

        assertThat(shortUrlDao.findByShortCode(shortUrl.getShortCode()))
                .isPresent()
                .hasValueSatisfying(sc -> {
                    assertThat(sc).isEqualToComparingFieldByFieldRecursively(shortUrl);
                });
    }

    @Test
    @Transactional
    public void registerRedirect() {
        final String url = RandomStringUtils.randomAlphanumeric(20);
        final int redirectType = 301;

        ShortUrl shortUrl = shortUrlDao.register(account, url, "Da2Baeve", redirectType);

        shortUrlDao.registerRedirect(shortUrl.getShortCode());

        assertThat(new Request(currentTransactionDataSource(dataSource), "SELECT * FROM short_urls WHERE short_code=?", shortUrl.getShortCode()))
                .hasNumberOfRows(1)
                .column("statistic").value().isEqualTo(1);
    }

    @Test
    @Transactional
    public void getStatisticByOwner() {
        final String url1 = RandomStringUtils.randomAlphanumeric(20);
        final String url2 = RandomStringUtils.randomAlphanumeric(20);

        ShortUrl shortUrl1 = shortUrlDao.register(account, url1, "url1", 301);
        ShortUrl shortUrl2 = shortUrlDao.register(account, url2, "url2", 301);

        // same url as url1
        ShortUrl shortUrl3 = shortUrlDao.register(account, url1, "url3", 301);

        for (int i = 0; i < 5; i++) {
            shortUrlDao.registerRedirect(shortUrl1.getShortCode());
            shortUrlDao.registerRedirect(shortUrl2.getShortCode());
            shortUrlDao.registerRedirect(shortUrl3.getShortCode());
        }

        assertThat(shortUrlDao.getStatisticByOwner(account))
                .hasSize(2)
                .containsOnly(entry(url1, 10L), entry(url2, 5L));
    }
}