package ru.ind.infobip.services.accountregistration;

import org.assertj.db.type.Request;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.ind.infobip.services.accountregistration.dao.AccountDao;
import ru.ind.infobip.services.accountregistration.model.Account;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.db.api.Assertions.assertThat;
import static ru.ind.infobip.TestUtil.currentTransactionDataSource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountRegistrationDaoIT {
    @Inject
    private AccountDao accountRegistrationDao;

    @Inject
    private DataSource dataSource;

    @Test
    @Transactional
    public void insert() throws Exception {
        final Account account = accountRegistrationDao.insert("123", "passwd");

        assertThat(new Request(currentTransactionDataSource(dataSource), "SELECT * FROM accounts WHERE id=?", account.getId()))
                .hasNumberOfRows(1)
                .column("account_id").value().isEqualTo(account.getAccountId())
                .column("password").value().isEqualTo(account.getPassword());
    }

    @Test
    @Transactional
    @Sql(statements = "INSERT INTO accounts (account_id, password) VALUES ('123', 'asd')", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    public void find() {
        final Optional<Account> accountOptional = accountRegistrationDao.findAccount("123");
        assertThat(accountOptional)
                .isPresent()
                .hasValueSatisfying(a -> {
                    assertThat(a.getAccountId()).isEqualTo("123");
                    assertThat(a.getPassword()).isEqualTo("asd");
                });
    }
}