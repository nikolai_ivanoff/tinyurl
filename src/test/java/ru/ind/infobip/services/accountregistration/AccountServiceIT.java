package ru.ind.infobip.services.accountregistration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.ind.infobip.services.accountregistration.model.Account;

import javax.inject.Inject;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "account.password-length=16")
public class AccountServiceIT {
    @Inject
    private AccountService accountService;

    @Inject
    private PasswordEncoder passwordEncoder;

    @Test
    @Transactional
    public void registerAccount() throws Exception {
        final Account test = accountService.registerAccount("test");

        assertThat(test.getId())
                .isPositive();
        assertThat(test.getAccountId())
                .isEqualTo("test");
        assertThat(test.getRawPassword())
                .hasSize(16);

        assertThat(passwordEncoder.matches(test.getRawPassword(), test.getPassword()))
                .isTrue();
    }

    @Test
    @Transactional
    public void findAccount() throws Exception {
        final Account test = accountService.registerAccount("test");

        assertThat(accountService.findAccount("test"))
                .isPresent()
                .hasValueSatisfying(a -> {
                    assertThat(a).isEqualToIgnoringGivenFields(test, "rawPassword");
                });
    }

    @Test
    @Transactional
    public void findAccountWhichNotExist() throws Exception {
        assertThat(accountService.findAccount("absent-account-id"))
                .isEmpty();
    }
}