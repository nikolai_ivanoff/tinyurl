package ru.ind.infobip.web.controllers.shorturl;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Base64Utils;
import ru.ind.infobip.services.accountregistration.AccountService;
import ru.ind.infobip.services.accountregistration.model.Account;
import ru.ind.infobip.services.shorturl.ShortUrlService;

import javax.inject.Inject;
import java.util.Optional;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StatisticControllerIT {
    @Inject
    private MockMvc mvc;

    @Inject
    private PasswordEncoder passwordEncoder;

    @MockBean
    private AccountService accountService;

    @MockBean
    private ShortUrlService shortUrlService;

    @Test
    public void statisticRequireAuth() throws Exception {
        this.mvc.perform(get("/statistic/asd"))
                .andExpect(status().isUnauthorized());

        verifyZeroInteractions(shortUrlService);
    }

    @Test
    public void statistic() throws Exception {
        final Account account = new Account(1, "account", passwordEncoder.encode("secret"));
        when(accountService.findAccount(eq("account")))
                .thenReturn(Optional.of(account));

        when(shortUrlService.getStatisticByOwner(eq(account)))
                .thenReturn(ImmutableMap.of("http://google.com", 33L, "http://https://stackoverflow.com", 5L));

        mvc.perform(get("/statistic/account")
                .header(HttpHeaders.AUTHORIZATION, "Basic " + Base64Utils.encodeToString("account:secret".getBytes())))
                .andExpect(status().isOk())
                .andExpect(content().json("{ \"http://google.com\": 33, \"http://https://stackoverflow.com\": 5}"));
    }
}