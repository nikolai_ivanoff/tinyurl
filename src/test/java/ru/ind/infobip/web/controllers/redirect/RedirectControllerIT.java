package ru.ind.infobip.web.controllers.redirect;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.ind.infobip.services.shorturl.ShortUrlService;
import ru.ind.infobip.services.shorturl.model.ShortUrl;

import javax.inject.Inject;
import java.util.Optional;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RedirectControllerIT {
    @Inject
    private MockMvc mvc;

    @MockBean
    private ShortUrlService shortUrlService;

    @Test
    public void redirectForAbsentShortCode() throws Exception {
        when(shortUrlService.findByShortUrlCode(anyString()))
                .thenReturn(Optional.empty());

        mvc.perform(get("/qwerty"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void redirect() throws Exception {
        when(shortUrlService.findByShortUrlCode("qwerty"))
                .thenReturn(Optional.of(new ShortUrl("qwerty", 1, "http://google.com", 301, 0)));

        mvc.perform(get("/qwerty"))
                .andExpect(status().isMovedPermanently())
                .andExpect(redirectedUrl("http://google.com"));

        verify(shortUrlService)
                .findByShortUrlCode(eq("qwerty"));
        verify(shortUrlService)
                .registerRedirect(eq("qwerty"));

    }
}