package ru.ind.infobip.web.controllers.account;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import ru.ind.infobip.services.accountregistration.AccountService;
import ru.ind.infobip.services.accountregistration.model.Account;

import javax.inject.Inject;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AccountRegistrationControllerIT {
    @Inject
    private MockMvc mvc;

    @MockBean
    private AccountService accountService;

    @Test
    public void registerAccountSuccess() throws Exception {
        final String accountId = "myAccountId";
        when(accountService.registerAccount(eq(accountId)))
                .thenReturn(new Account(1, accountId, "encrypted-passwd").setRawPassword("xC345Fc0"));

        final MockHttpServletRequestBuilder request = post("/account")
                .content("{ \"AccountId\" : \"myAccountId\" }")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);

        mvc.perform(request)
                .andExpect(status().isCreated())
                .andExpect(content().json("{success: true, description: 'Your account is opened', password: 'xC345Fc0'}"));
    }

    @Test
    public void registerAccountFailure() throws Exception {
        when(accountService.registerAccount(any(String.class)))
                .thenThrow(new RuntimeException("some error"));

        final MockHttpServletRequestBuilder request = post("/account")
                .content("{ AccountId : 'myAccountId' }")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);

        mvc.perform(request)
                .andExpect(status().is(INTERNAL_SERVER_ERROR.value()))
                .andExpect(jsonPath("$.success").value(false))
                .andExpect(jsonPath("$.description").isNotEmpty())
                .andExpect(jsonPath("$.password").doesNotExist());
    }
}