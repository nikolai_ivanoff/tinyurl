package ru.ind.infobip.web.controllers.shorturl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.util.Base64Utils;
import ru.ind.infobip.services.accountregistration.AccountService;
import ru.ind.infobip.services.accountregistration.model.Account;
import ru.ind.infobip.services.shorturl.ShortUrlService;
import ru.ind.infobip.services.shorturl.model.ShortUrl;

import javax.inject.Inject;
import java.util.Optional;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "short-url-prefix=http://localhost/")
@AutoConfigureMockMvc
public class ShortUrlRegistrationControllerIT {
    @Inject
    private MockMvc mvc;

    @MockBean
    private AccountService accountService;

    @MockBean
    private ShortUrlService shortUrlService;

    @Inject
    private PasswordEncoder passwordEncoder;

    @Test
    public void registerUrlRequireAuth() throws Exception {
        final MockHttpServletRequestBuilder request = post("/register")
                .content("{ \"url\": \"http://google.com\" }")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);

        this.mvc.perform(request)
                .andExpect(status().isUnauthorized());

        verifyZeroInteractions(shortUrlService);
    }

    @Test
    public void registerUrl() throws Exception {
        final Account account = new Account(1, "account", passwordEncoder.encode("secret"));
        when(accountService.findAccount(eq("account")))
                .thenReturn(Optional.of(account));

        when(shortUrlService.register(any(Account.class), anyString(), anyInt()))
                .thenReturn(new ShortUrl("xyswle", account.getId(), "http://google.com", 302, 0));

        final MockHttpServletRequestBuilder request = post("/register")
                .header(HttpHeaders.AUTHORIZATION, "Basic " + Base64Utils.encodeToString("account:secret".getBytes()))
                .content("{ \"url\": \"http://google.com\" }")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);

        this.mvc.perform(request)
                .andExpect(status().isCreated())
                .andExpect(content().json("{ shortUrl: 'http://localhost/xyswle' }"));

        verify(shortUrlService)
                .register(eq(account), eq("http://google.com"), eq(302));
    }

    @Test
    public void registerUrlFailBadURL() throws Exception {
        final Account account = new Account(1, "account", passwordEncoder.encode("secret"));
        when(accountService.findAccount(eq("account")))
                .thenReturn(Optional.of(account));

        final MockHttpServletRequestBuilder request = post("/register")
                .header(HttpHeaders.AUTHORIZATION, "Basic " + Base64Utils.encodeToString("account:secret".getBytes()))
                .content("{ \"url\": \"local:host\" }")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);

        this.mvc.perform(request)
                .andExpect(status().isBadRequest());

        verifyZeroInteractions(shortUrlService);
    }

    @Test
    public void registerUrlFailBadRedirectType() throws Exception {
        final Account account = new Account(1, "account", passwordEncoder.encode("secret"));
        when(accountService.findAccount(eq("account")))
                .thenReturn(Optional.of(account));

        final MockHttpServletRequestBuilder request = post("/register")
                .header(HttpHeaders.AUTHORIZATION, "Basic " + Base64Utils.encodeToString("account:secret".getBytes()))
                .content("{ \"url\": \"http://localhost\", \"redirectType\": 303 }")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);

        this.mvc.perform(request)
                .andExpect(status().isBadRequest());

        verifyZeroInteractions(shortUrlService);
    }
}